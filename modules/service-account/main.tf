resource "google_service_account" "terraformUser" {
  account_id   = "terraformuser"
  display_name = "Terraform User"
  project      = "${var.project}"
}

data "google_iam_policy" "projectadmin" {
  binding {
    role = "roles/resourcemanager.projects.create"

    members = [
      "serviceAccount:${var.serviceAccount}",
    ]
  }

  # resourcemanager.projects.create
  # resourcemanager.projects.delete
  # resourcemanager.projects.get
  # resourcemanager.projects.getIamPolicy
  # resourcemanager.projects.list
  # resourcemanager.projects.move
  # resourcemanager.projects.setIamPolicy
  # resourcemanager.projects.update
}
