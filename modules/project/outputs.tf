output "project_id" {
  value       = "${google_project.project.project_id}"
  description = "Created Project ID"
}

output "folder_final_name" {
  value       = "${google_folder.parent_folder.name}"
  description = "Created Folder Name"
}
