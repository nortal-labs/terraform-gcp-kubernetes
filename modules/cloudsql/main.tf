# CloudSQL instance
resource "google_sql_database" "default" {
  name      = "${var.database_name}"
  project   = "${var.project}"
  instance  = "${google_sql_database_instance.master.name}"
  charset   = "${var.db_charset}"
  collation = "${var.db_collation}"
}

# Actual database instance
resource "google_sql_database_instance" "master" {
  name             = "${var.database_name}"
  database_version = "${var.dbversion}"
  region           = "${var.region}"
  project          = "${var.project}"

  settings {
    tier              = "${var.tier}"
    activation_policy = "${var.activation_policy}"
    disk_size         = "${var.disk_size}"

    ip_configuration {
      ipv4_enabled = true

      # require_ssl = true
      authorized_networks = {
        name            = "Everyone Network"
        value           = "0.0.0.0/0"
        expiration_time = "2018-04-12T16:20:00.094Z"
      }
    }
  }
}

resource "google_sql_user" "users" {
  project  = "${var.project}"
  instance = "${google_sql_database_instance.master.name}"
  name     = "${var.dbuser}"
  host     = "${var.dbhost}"
  password = "${var.dbpassword}"
}
